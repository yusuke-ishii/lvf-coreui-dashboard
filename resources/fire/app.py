#!python3
import json

class App:


    # out関数を通して結果をJSON形式で出力することでLaravelに結果を返す。
    def out(self, res_obj):
        json_str = json.dumps(res_obj)
        print(json_str)


    # サンプル関数
    def hello(self, name=""):
        res = {
            "data": "hello " + name
        }
        self.out(res)


if __name__ == '__main__':
    import fire
    fire.Fire(App)
