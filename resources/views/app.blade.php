
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    @if(app('env')=='local')
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @endif
    @if(app('env')=='production')
        <link href="{{ mix(secure_asset('css/app.css')) }}" rel="stylesheet">
    @endif

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<body>
    <div id=app></div>
    @if(app('env')=='local')
        <script src="{{ asset('js/app.js') }}"></script>
    @endif
    @if(app('env')=='production')
        <script src="{{ mix(secure_asset('js/app.js')) }}"></script>
    @endif
</body>

</html>
