<?php

namespace App\Libraries\Fire;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;


class PythonFireWrapper
{

    private $app_path;
    private $bin = 'python3';


    function __construct($app_path='/resources/fire/app.py')
    {
        $this->app_path = base_path() . $app_path;
    }


    public function __call($name, $arguments)
    {
        if(is_array($arguments)){
            $arguments = join(' ', $arguments);
        }
        $bin = $this->bin;
        $app_path = $this->app_path;
        $process = new Process("${bin} ${app_path} ${name} ${arguments}");
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $res = $process->getOutput();
        return json_decode($res);
    }


}
